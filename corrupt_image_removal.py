#__author__ = 'gerry'
#verify if an image is corrupt or not
#help from https://stackoverflow.com/questions/3964681/find-all-files-in-a-directory-with-extension-txt-in-python

img_dir = "C:\\Users\\Nishant\\Desktop\\imp\\X\\Apparels-Women-Western Wear-Shirts Tops n Tunics-Tunics"

corrupt_img_dir="C:\\Users\\Nishant\\Desktop\\imp\\X\\Apparels-Women-Western Wear-Shirts Tops n Tunics-Tunics"
good_img_dir="C:\\Users\\Nishant\\Desktop\\imp\\X\\New folder"


from PIL import Image
import os,time
i=0
Image.MAX_IMAGE_PIXELS = 933120000
def verify_image(img_file):
     #test image
     try:
        v_image = Image.open(img_file)
        v_image.verify()
        return True;
        #is valid
        #print("valid file: "+img_file)
     except (OSError, IOError ) as e:
        return False;


#main script
for root, dirs, files in os.walk(img_dir):
    for file in files:
        if file.endswith(".jpg"):
             currentFile=os.path.join(root, file)
             #test image
             if verify_image(currentFile):
                 new_file_name=good_img_dir+time.strftime("%Y%m%d%H%M%S_"+os.path.basename(currentFile))
                 print("good file, moving to dir: "+new_file_name)
                 try:
                     os.rename(currentFile, file)
                 except WindowsError:
                     print("error moving file")
             else:
                 #Move to corrupt folder
                 #makefilename unique
                 #new_file_name=corrupt_img_dir+time.strftime("%Y%m%d%H%M%S_"+os.path.basename(currentFile))
                 print("corrupt file"+str(i))
                 i = i+1
                 #os.rename(currentFile, new_file_name)
