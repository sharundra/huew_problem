import os 
from os import listdir
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img

datagen = ImageDataGenerator(
        rotation_range=50,
        width_shift_range=0.15,
        height_shift_range=0.15,
        shear_range=0.2, 
        zoom_range=0.2,
        #featurewise_center=True,
        #horizontal_flip=True,
        fill_mode='constant')
directory = "C:\Users\Nishant\Desktop\imp\y\zem\Apparels-Women-Ethnic Wear-Dupattas"
nam = "Dupattas"
k = 0
for filename in os.listdir(directory):
  if filename.endswith(".jpeg") or filename.endswith("jpg") or filename.endswith(".py"):
    img = load_img(directory+'/'+filename)  # this is a PIL image
    #print(filename)
    x = img_to_array(img)  # this is a Numpy array with shape (3, 150, 150)
    x = x.reshape((1,) + x.shape)  # this is a Numpy array with shape (1, 3, 150, 150)
    # the .flow() command below generates batches of randomly transformed images
    # and saves the results to the `preview/` directory
    i = 0
    for batch in datagen.flow(x, batch_size=1,save_to_dir="C:\Users\Nishant\Desktop\imp\y\zem\aug", save_prefix=nam, save_format='jpg'):
      i += 1 
      if i > 1:
        break  # otherwise the generator would loop indefinitely
