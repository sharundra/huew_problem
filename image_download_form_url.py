#!/usr/bin/env python

# assuming a csv file with a name in column 0 and the image url in column 1

#import urllib
import requests
filename = "images"

# open file to read
with open("{0}.csv".format("csv_file_name"), 'r') as csvfile:
    # iterate on all lines
    i = 0
    for line in csvfile:
        splitted_line = line.split(',')
        # check if we have an image URL
        if splitted_line[1] != '' and splitted_line[1] != "\n":
            try:
                url = splitted_line[1]
                filename = splitted_line[0] +"_"+str(i+34647)+ ".jpg"
                rep = requests.get(url, allow_redirects = True)
                #urllib.urlretrieve(splitted_line[1], splitted_line[0] +"_"+str(i+18546)+ ".jpg")
                #resp = urllib2.urlopen(splitted_line[1])
                open(filename, 'wb').write(rep.content)
                #with open(sav_name, 'wb') as f:
                    #f.write(resp.read())
                print ("Image saved for {0}".format(splitted_line[0])
            except:
                print("an error occured, continuing")
            i += 1
        else:
            print ("No result for {0}".format(splitted_line[0]))



